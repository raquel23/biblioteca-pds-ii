import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JTabbedPane;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;

public class Biblioteca extends JFrame {
	private JTextField textField_titulo;
	private JTextField textField_genero;
	private JTextField textField_editora;
	private JTextField txtNome_Usr;
	private JTextField txtSobrenome;
	private JTextField txtEmail;
	private JTextField txt_cpf;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Biblioteca frame = new Biblioteca();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Biblioteca() {
		setTitle("Livros\r\n");
		getContentPane().setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 443, 285);
		getContentPane().add(tabbedPane);

		JPanel pLivros = new JPanel();
		tabbedPane.addTab("Livros", null, pLivros, null);
		pLivros.setLayout(null);

		textField_titulo = new JTextField();
		textField_titulo.setFont(new Font("Georgia", Font.PLAIN, 12));
		textField_titulo.setColumns(10);
		textField_titulo.setBounds(144, 34, 158, 20);
		pLivros.add(textField_titulo);

		textField_genero = new JTextField();
		textField_genero.setFont(new Font("Georgia", Font.PLAIN, 12));
		textField_genero.setColumns(10);
		textField_genero.setBounds(144, 122, 158, 20);
		pLivros.add(textField_genero);

		textField_editora = new JTextField();
		textField_editora.setFont(new Font("Georgia", Font.PLAIN, 12));
		textField_editora.setColumns(10);
		textField_editora.setBounds(144, 165, 158, 20);
		pLivros.add(textField_editora);
		
		JComboBox comboBoxAutor = new JComboBox();
		comboBoxAutor.setToolTipText("");
		comboBoxAutor.setBounds(144, 79, 158, 20);
		pLivros.add(comboBoxAutor);
		
		JLabel label_3 = new JLabel("T\u00EDtulo:");
		label_3.setFont(new Font("Georgia", Font.PLAIN, 12));
		label_3.setBounds(44, 37, 46, 14);
		pLivros.add(label_3);
		
		JLabel label_2 = new JLabel("Autor:");
		label_2.setFont(new Font("Georgia", Font.PLAIN, 12));
		label_2.setBounds(44, 82, 46, 14);
		pLivros.add(label_2);
		
		JLabel label_1 = new JLabel("G\u00EAnero:");
		label_1.setFont(new Font("Georgia", Font.PLAIN, 12));
		label_1.setBounds(44, 125, 46, 14);
		pLivros.add(label_1);
		
		JLabel label = new JLabel("Editora");
		label.setFont(new Font("Georgia", Font.PLAIN, 12));
		label.setBounds(44, 168, 46, 14);
		pLivros.add(label);
		
		

		JLabel label_4 = new JLabel("Livros");
		label_4.setFont(new Font("Georgia", Font.BOLD, 14));
		label_4.setBounds(187, 1, 55, 22);
		pLivros.add(label_4);


		JButton btn_Listar = new JButton("Listar");
		btn_Listar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btn_Listar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						String titulo = textField_titulo.getText();
						String autor = comboBoxAutor.getSelectedItem().toString();;
						String genero = textField_genero.getText();
						String editora = textField_editora.getText();
						
						try {
							Connection conexao = DriverManager.getConnection("jdbc:mysql://localhost:3306/?user=root&password=aluno");

							Statement Listar = conexao.createStatement();

							String listar = "SELECT * FROM biblioteca.livros";
						

							ResultSet lista = Listar.executeQuery(listar);

							while (lista.next()) {
								JOptionPane.showMessageDialog(null,"t�tulo: " + lista.getString("titulo") );
								JOptionPane.showMessageDialog(null,"autor: " + lista.getString("autor"));
								JOptionPane.showMessageDialog(null,"g�nero" + lista.getString("genero"));
								JOptionPane.showMessageDialog(null,"editora" + lista.getString("editora"));

							}

						} catch (SQLException ex) {
							ex.printStackTrace();
						}

					}
				});
			}
		});
		btn_Listar.setFont(new Font("Georgia", Font.PLAIN, 12));
		btn_Listar.setBounds(328, 225, 96, 23);
		pLivros.add(btn_Listar);

		JButton btn_excluir = new JButton("Excluir");
		btn_excluir.setFont(new Font("Georgia", Font.PLAIN, 12));
		btn_excluir.setBounds(222, 225, 96, 23);
		pLivros.add(btn_excluir);
		btn_excluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String titulo = textField_titulo.getText();
				String autor = comboBoxAutor.getSelectedItem().toString();;
				
				try {
					Connection conexao = DriverManager
							.getConnection("jdbc:mysql://localhost:3306/?user=root&password=aluno");

					Statement stConsulta = conexao.createStatement();

					String excluir = "DELETE FROM biblioteca.livros WHERE titulo = '" + titulo + "' " + "AND autor = '"
							+ autor + "'";
					JOptionPane.showMessageDialog(null, "excluindo livro " + titulo + "  do autor" + autor);
					stConsulta.execute(excluir);

				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		JButton btn_Atualizar = new JButton("Atualizar");
		btn_Atualizar.setFont(new Font("Georgia", Font.PLAIN, 12));
		btn_Atualizar.setBounds(116, 225, 96, 23);
		pLivros.add(btn_Atualizar);
		btn_Atualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					// atualiza o titulo 
					
					String titulo1  = JOptionPane.showInputDialog("Insira o t�tulo da obra");

					String titulo2 = JOptionPane.showInputDialog("Insira o t�tulo atualizado");

					Connection conexao = DriverManager
							.getConnection("jdbc:mysql://localhost:3306/?user=root&password=aluno");

					Statement stConsulta = conexao.createStatement();

					String consulta = "UPDATE biblioteca.livros SET titulo = '" + titulo2 + "' " + "WHERE titulo = '"
							+ titulo1 + "'";
					JOptionPane.showMessageDialog(null, "Atualizado com sucesso!");
					stConsulta.execute(consulta);

				} catch (SQLException exc) {
					exc.printStackTrace();
				}
			}

		});

		JButton btn_Cadastrar = new JButton("Cadastrar");
		btn_Cadastrar.setFont(new Font("Georgia", Font.PLAIN, 12));
		btn_Cadastrar.setBounds(10, 225, 96, 23);
		pLivros.add(btn_Cadastrar);
		
		btn_Cadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
		
				String titulo = textField_titulo.getText();
				String autor = comboBoxAutor.getSelectedItem().toString();;
				String genero = textField_genero.getText();
				String editora = textField_editora.getText();
				
				try{
				Connection conexao = DriverManager.getConnection("jdbc:mysql://localhost:3306/?user=root&password=aluno");
				Statement cadastro = conexao.createStatement();
				
				String cadastrar = "INSERT INTO biblioteca.livros (titulo,autor, genero, editora)"
						+ "VALUES ('"+titulo+"','"+autor+"','"+genero+"','"+editora+"')";
				
				cadastro.execute(cadastrar);
				JOptionPane.showMessageDialog(null, "Cadastro efetuado!");
				}catch (SQLException e){
					e.printStackTrace();
				}

			}
		});

	
		JPanel pUsuario = new JPanel();
		tabbedPane.addTab("Usu\u00E1rio", null, pUsuario, null);
		pUsuario.setLayout(null);

		JLabel lblUsuario = new JLabel("Usu\u00E1rio");
		lblUsuario.setBounds(185, 11, 67, 17);
		lblUsuario.setFont(new Font("Georgia", Font.BOLD, 14));
		pUsuario.add(lblUsuario);

		txtNome_Usr = new JTextField();
		txtNome_Usr.setBounds(152, 49, 126, 20);
		txtNome_Usr.setFont(new Font("Georgia", Font.PLAIN, 12));
		txtNome_Usr.setColumns(10);
		pUsuario.add(txtNome_Usr);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setFont(new Font("Georgia", Font.PLAIN, 12));
		lblNome.setBounds(42, 52, 46, 14);
		pUsuario.add(lblNome);
		
		JLabel lblSobrenome = new JLabel("Sobrenome:");
		lblSobrenome.setFont(new Font("Georgia", Font.PLAIN, 12));
		lblSobrenome.setBounds(42, 96, 80, 14);
		pUsuario.add(lblSobrenome);

		txtSobrenome = new JTextField();
		txtSobrenome.setFont(new Font("Georgia", Font.PLAIN, 12));
		txtSobrenome.setColumns(10);
		txtSobrenome.setBounds(152, 93, 126, 20);
		pUsuario.add(txtSobrenome);

		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setFont(new Font("Georgia", Font.PLAIN, 12));
		lblEmail.setBounds(42, 140, 80, 14);
		pUsuario.add(lblEmail);

		txtEmail = new JTextField();
		txtEmail.setFont(new Font("Georgia", Font.PLAIN, 12));
		txtEmail.setColumns(10);
		txtEmail.setBounds(152, 137, 126, 20);
		pUsuario.add(txtEmail);

		JLabel lblCPF = new JLabel("CPF:");
		lblCPF.setHorizontalAlignment(SwingConstants.LEFT);
		lblCPF.setFont(new Font("Georgia", Font.PLAIN, 12));
		lblCPF.setBounds(42, 179, 51, 14);
		pUsuario.add(lblCPF);

		txt_cpf = new JTextField();
		txt_cpf.setFont(new Font("Georgia", Font.PLAIN, 12));
		txt_cpf.setColumns(10);
		txt_cpf.setBounds(152, 176, 126, 20);
		pUsuario.add(txt_cpf);

		JButton button_Cadastrar = new JButton("Cadastrar");
		button_Cadastrar.setBounds(10, 223, 91, 23);
		button_Cadastrar.setFont(new Font("Georgia", Font.PLAIN, 12));
		pUsuario.add(button_Cadastrar);
		button_Cadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
		
				String nome = txtNome_Usr.getText();
				String sobrenome = txtSobrenome.getText();
				String email = txtEmail.getText();
				String cpf = txt_cpf.getText();
				
				try{
				Connection conexao = DriverManager.getConnection("jdbc:mysql://localhost:3306/?user=root&password=aluno");
				Statement cadastro = conexao.createStatement();
				
				String cadastrar = "INSERT INTO biblioteca.usuarios (nome,sobrenome, email,cpf)"
						+ "VALUES ('"+nome+"','"+sobrenome+"','"+email+"','"+cpf+"')";
				
				cadastro.execute(cadastrar);
				JOptionPane.showMessageDialog(null, "Cadastro efetuado!");
				}catch (SQLException e){
					e.printStackTrace();
					
				}

			}
		});

		JButton button_Atualizar = new JButton("Atualizar");
		button_Atualizar.setFont(new Font("Georgia", Font.PLAIN, 12));
		button_Atualizar.setBounds(111, 223, 96, 23);
		pUsuario.add(button_Atualizar);
		btn_Atualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					// atualiza o nome pelo cpf do usu�rio
					
					String cpf = JOptionPane.showInputDialog("Insira o CPF do usu�rio");

					String nome = JOptionPane.showInputDialog("Insira o nome atualizado");

					Connection conexao = DriverManager.getConnection("jdbc:mysql://localhost:3306/?user=root&password=aluno");

					Statement stConsulta = conexao.createStatement();

					String consulta = "UPDATE biblioteca.usuarios SET nome = '" + nome + "' " + "WHERE cpf = '"
							+ cpf + "'";
					JOptionPane.showMessageDialog(null, "Atualizado com sucesso!");
					stConsulta.execute(consulta);

				} catch (SQLException exc) {
					exc.printStackTrace();
				}
			}

		});
		
		JButton button_Excluir = new JButton("Excluir");
		button_Excluir.setFont(new Font("Georgia", Font.PLAIN, 12));
		button_Excluir.setBounds(217, 223, 96, 23);
		pUsuario.add(button_Excluir);
		btn_excluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String cpf = txt_cpf.getText();
				String nome =  txtNome_Usr.getText();
				
				try {
					Connection conexao = DriverManager.getConnection("jdbc:mysql://localhost:3306/?user=root&password=aluno");

					Statement stConsulta = conexao.createStatement();

					String excluir = "DELETE FROM biblioteca.alunos WHERE cpf = '" + cpf + "' " + "AND nome = '"+ nome + "'";
					JOptionPane.showMessageDialog(null, "excluindo usu�rio " + nome + "  portador do CPF" + cpf);
					stConsulta.execute(excluir);

				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		JButton button_Listar = new JButton("Listar");
		button_Listar.setFont(new Font("Georgia", Font.PLAIN, 12));
		button_Listar.setBounds(323, 223, 96, 23);
		pUsuario.add(button_Listar);
		button_Listar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				String nome =  txtNome_Usr.getText();
				String sobrenome = txtSobrenome.getText();
				String cpf = txt_cpf.getText();
				String email = txtEmail.getText();
				
				try {
					Connection conexao = DriverManager
							.getConnection("jdbc:mysql://localhost:3306/?user=root&password=aluno");

					Statement Listar = conexao.createStatement();

					String listar = "SELECT * FROM biblioteca.usuarios";
				

					ResultSet lista = Listar.executeQuery(listar);

					while (lista.next()) {
						JOptionPane.showMessageDialog(null,"nome: " + lista.getString("nome") );
						JOptionPane.showMessageDialog(null,"sobrenome: " + lista.getString("sobrenome"));
						JOptionPane.showMessageDialog(null,"cpf:" + lista.getString("cpf"));
						JOptionPane.showMessageDialog(null,"email" + lista.getString("email"));
					}

				} catch (SQLException ex) {
					ex.printStackTrace();
				}

			}
		});
		
		

	}
}
